webhelpers (1.3-6) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Fri, 18 Oct 2019 15:27:43 +0200

webhelpers (1.3-5) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Gianfranco Costamagna ]
  * Override json evil lintian error (Closes: #794866, Closes: #760655)
  * Team Upload
  * Bump std-version to 4.4.0, no changes required
  * Bump compat level to 12

  [ Santiago Vila ]
  * Fix test failure when timezone changes (Closes: #842448)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 12 Aug 2019 11:20:11 +0200

webhelpers (1.3-4) unstable; urgency=low

  * Remove WebHelpers.egg-info in clean target (to make it build twice in a
    row with DEB_BUILD_OPTIONS=nocheck, closes: 671532)
  * Add (now empty) build-arch and build-indep targets to debian/rules
  * Update Homepage field (closes: 637886)
  * Standards-Version bumped to 3.9.3 (no other changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 08 Jul 2012 22:27:47 +0200

webhelpers (1.3-3) unstable; urgency=low

  * Handle API change in WebOb 1.1.1 (thanks to Colin Watson for the patch
    from Ubuntu)
  * Disable distance_of_time_in_words tests (Closes: 629699)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 07 Jul 2012 19:13:26 +0200

webhelpers (1.3-2) unstable; urgency=low

  [ Andrey Rahmatullin ]
  * Enable build-time tests

  [ Piotr Ożarowski ]
  * Add python-utidylib to Suggests (for webhelpers.textile)
  * Standards-Version bumped to 3.9.2 (no other changes needed)
  * Source format changed to 3.0 (quilt)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 07 May 2011 21:20:00 +0200

webhelpers (1.3-1) unstable; urgency=low

  * New upstream release
  * Switch from dh_pysupport to dh_python2

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 26 Mar 2011 10:18:37 +0100

webhelpers (1.2-1) experimental; urgency=low

  * New upstream release
  * python-markupsafe added to Depends

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 21 Aug 2010 00:46:36 +0200

webhelpers (1.1-1) unstable; urgency=low

  * New upstream release
    - webhelpers.pylonslib._jsmin module removed due to licensing issues
  * debian/copyright file updated
  * Standards-Version bumped to 3.9.1:
    - BSD license included directly in debian/copyright

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 13 Aug 2010 18:58:48 +0200

webhelpers (1.0-1) unstable; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 07 Jun 2010 21:37:50 +0200

webhelpers (1.0~rc1-1) unstable; urgency=low

  * New upstream release
  * debian/watch file updated to handle "rc" versions

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 25 May 2010 20:33:32 +0200

webhelpers (1.0~b7-1) unstable; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 16 May 2010 20:58:33 +0200

webhelpers (1.0~b6-1) unstable; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 24 Apr 2010 11:08:25 +0200

webhelpers (1.0~b5-1) unstable; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 23 Mar 2010 19:39:54 +0100

webhelpers (1.0~b4-1) unstable; urgency=low

  [ Piotr Ożarowski ]
  * New upstream release
    - bump minimum required Python version to 2.4
  * debian/watch updated to recognize beta versions correctly
  * Standards-Version bumped to 3.8.4 (no changes needed)

  [ Oleksandr Moskalenko ]
  * debian/control:
    - Changed to DPMT group maintenance and added Piotr to the Uploaders.
    - Moved python depends to Build-Depends-Indep.
  * debian/copyright: Changed debian packaging license to BSD to sync with the
    upstream.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 18 Feb 2010 15:46:38 -0600

webhelpers (1.0~b3-1) experimental; urgency=low

  * Upload the third beta to experimental.
  * debian/source/format: Change source format to v 3.0 (quilt).
  * debian/rules:
    - Remove obsolete patching code.
    - Remove obsolete linking to scriptaculous.
  * debian/control: Remove build-depends on dpatch.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 07 Jan 2010 23:56:08 -0600

webhelpers (0.6.4-2) unstable; urgency=low

  * Acknowledge NMUs.
  * debian/control: Updated standards-version to 3.8.3.
  * Jakub Wilk <ubanus@users.sf.net>:
    + Use the system-wide scriptaculous javascript library.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 07 Jan 2010 23:48:50 -0600

webhelpers (0.6.4-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Build-Depend on python-all-dev (>= 2.5.4) for python.mk.

 -- Kumar Appaiah <akumar@debian.org>  Mon, 19 Oct 2009 17:35:21 -0500

webhelpers (0.6.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "manipulates site-packages/ directly, failing with Python 2.6"
    Applied and uploaded Kumar's patch, thanks dude! (Closes: #547862)

 -- Bastian Venthur <venthur@debian.org>  Sat, 17 Oct 2009 14:52:35 +0200

webhelpers (0.6.4-1) unstable; urgency=low

  * New upstream release.
  [Sandro Tosi <morph@debian.org>]:
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 01 Dec 2008 18:00:00 -0700

webhelpers (0.6.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Removed build-depends on python as pysupport doesn't require it now.
    - Removed XS-Python-Version: all as pysupport doesn't require it now.
    - Removed python from Depends as pysupport doesn't require it now.
    - Removed XB-Python-Version: ${python:Versions} as pysupport doesn't
      require it now.
    - Removed Enhances: python-webhelpers as it doesn't make sense for a
      package to enhance itself.
  * debian/rules: Simplified dpatch patch handling rules.

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 08 Oct 2008 10:23:40 -0600

webhelpers (0.6.1-1) unstable; urgency=low

  * New upstream release.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 31 Jul 2008 17:17:33 -0600

webhelpers (0.6-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Updated Standards-Version to 3.8.0.

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 08 Jul 2008 17:48:02 -0600

webhelpers (0.3.4-2) unstable; urgency=low

  * Remove the javascript prototypes and link to and depend on libjs-prototype
    (Closes: #475291).
  * debian/rules:
    - Removed dh_strip clause as it is not needed for .py files.
    - Added a clause to remove prototype.js.
    - Added a clause to create a link to prototype.js from libjs-prototype.
  * debian/control:
    - Updated Standards-Version to 3.7.3.
    - Added a dependency on libjs-prototype.
    - Changed section to python per lintian warning.
    - Removed the -1 revision from dependency on python-setuptools.

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 30 Apr 2008 16:59:12 -0600

webhelpers (0.3.4-1) unstable; urgency=low

  Oleksandr Moskalenko:
  * New upstream release.
  * debian/patches/01_setup_py.dpatch: Remove this patch as the upstream uses
    setuptools by default now.
  * debian/patches/00list: Remove the reference to deleted 01_setup_py.dpatch.
  Piotr Ożarowski:
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now).

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 18 Mar 2008 13:10:42 -0600

webhelpers (0.3.1-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/01_setup_py.dpatch: Added a patch to remove ez_setup use
    (thanks to Piotr Ozarowski for the suggestion).
  * debian/patches/00list: Added 01_setup_py.dpatch to the list of patches to
    apply.
  * debian/rules: Removed a duplicate build-stamp rule.

 -- Oleksandr Moskalenko <malex@debian.org>  Sun, 15 Jul 2007 16:48:11 -0600

webhelpers (0.3-2) unstable; urgency=low

  * Piotr Ożarowski <piotr@debian.org>:
    + New python-support handles egg's directory name correctly.
      - bump python-support required version.
      - remove mv part from debian/rules.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 14 May 2007 10:01:56 -0600

webhelpers (0.3-1) unstable; urgency=low

  * New upstream version.
  * debian/patches/01_textile.py.dpatch: Removed the patch as my changes were
    incorporated upstream.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 29 Mar 2007 17:08:06 -0600

webhelpers (0.2.2-3) unstable; urgency=low

  * debian/control:
    - Uploaded the debian directory into Debian Python Modules Team subversion
      repository, so added <python-modules-team@lists.alioth.debian.org> to
      "Uploaders:".
    - Added XS-Vcs-Svn and XS-Vcs-Browser fields.

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 26 Jan 2007 16:24:35 -0700

webhelpers (0.2.2-2) unstable; urgency=low

  * debian/control: Added a dependency on dpatch.
  * debian/patches: Added the directory for the patches dpatch uses.
  * debian/rules: Added the patching rules.
  * debian/patches/01_textile.dpatch: Added a patch to change the encoding in
    textile.py (Closes: #408224).

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 26 Jan 2007 15:56:46 -0700

webhelpers (0.2.2-1) unstable; urgency=low

  * Initial release (Closes: #393757).

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 14 Nov 2006 10:17:21 -0700
